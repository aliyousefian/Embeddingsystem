

#define TFTG_DC 5
#define TFTG_CS 6
#define TFTD_DC 3
#define TFTD_CS 4

#define backlightTFTD 8 //(Backlight TFT sur pin digital 3 - reglage PWM de 0 a 255
#define backlightTFTG 9 //(Backlight TFT sur pin digital 3 - reglage PWM de 0 a 255

// X OFFSET OF DISPLAY SQUARES
#define X1 0
#define X2 108
#define X3 216
#define X4 324

#define BKLITE_NIGHT 10
#define BKLITE_DAY 100

#define XADJ 3 // x adjustment for displaying value (because even if perfectly centered, it seems too much on the left)

#define RED 0xF800
#define GREEN 0x7E0